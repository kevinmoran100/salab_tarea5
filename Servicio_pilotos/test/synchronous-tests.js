var assert = require('assert');
var server = require("../server");

var ejpiloto = { id: 'piloto1', name: 'Nombre piloto1', ocupado: false };

describe('Pilotos', function() {
  describe('#buscarPiloto2()', function() {
    it("Check the returned value using: value.should.equal(value): ", function() {
        result   = server.buscarPiloto2("piloto1");
        assert(JSON.stringify(result), JSON.stringify({ id: 'piloto1', name: 'Nombre piloto1', ocupado: false }));
  });
});
});